// // Or you can simply use a connection uri
// var sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname');
var dateFormat = require('dateformat');
var Sequelize = require('sequelize');

function getConnection() {
    // homologação
    // var sequelize = new Sequelize('robottj', 'root', '!@Fs20151*', {
    //     host: 'localhost',
    //     dialect: 'mysql', // |'mariadb'|'sqlite'|'postgres'|'mssql',
    //     pool: { max: 5, min: 0, idle: 10000 }
    // });

    // produção
    return new Sequelize('acsm_1a3d3e40a244599', 'bb99d24c15e0f0', 'a7add209', {
        host: 'br-cdbr-azure-south-b.cloudapp.net',
        port: 3306,
        dialect: 'mysql', // |'mariadb'|'sqlite'|'postgres'|'mssql',
        pool: { max: 5, min: 0, idle: 10000 }
    });
    // var sequelize = new Sequelize("Database=acsm_1a3d3e40a244599;Data Source=br-cdbr-azure-south-b.cloudapp.net;User Id=bb99d24c15e0f0;Password=a7add209");
}

module.exports = function() {
    var sequelize = getConnection();

    var Processo = sequelize.define('processo', {
        numero: { type: Sequelize.STRING }, // , allowNull: false },
        classe: { type: Sequelize.STRING }, // , allowNull: false },
        area: { type: Sequelize.STRING },
        assunto: { type: Sequelize.STRING }, // , allowNull: false },
        outros_assuntos: { type: Sequelize.STRING }, // novo*
        local_fisico: { type: Sequelize.STRING },
        data_distribuicao: { type: Sequelize.STRING }, // , allowNull: false },
        desc_distribuicao: { type: Sequelize.STRING },
        controle: { type: Sequelize.STRING }, // , allowNull: false },
        juiz: { type: Sequelize.STRING },
        valor_acao: { type: Sequelize.DOUBLE(18, 2) }, //, allowNull: false }
        situacao: { type: Sequelize.STRING } // novo*
    }, {
        freezeTableName: true,
        getterMethods: {
            data_atualizacao: function() {
                return dateFormat(this.updatedAt, "dd/mm/yyyy h:MM:ss");
            }
        }
    });

    var Parte = sequelize.define('parte', {
        tipo: { type: Sequelize.STRING }, // , allowNull: false },
        nome: { type: Sequelize.STRING }, // , allowNull: false },
        advogado: { type: Sequelize.STRING }
    }, {
        freezeTableName: true,
        getterMethods: {
            data_atualizacao: function() {
                return dateFormat(this.updatedAt, "dd/mm/yyyy h:MM:ss");
            }
        }
    });

    var Movimentacao = sequelize.define('movimentacao', {
        data: { type: Sequelize.DATE }, // , allowNull: false },
        descricao: { type: Sequelize.STRING }, // , allowNull: false },
        observacoes: { type: Sequelize.TEXT },
        url: { type: Sequelize.STRING }
    }, {
        freezeTableName: true,
        getterMethods: {
            data_movimentacao: function() {
                return dateFormat(this.data, "dd/mm/yyyy h:MM:ss");
            },
            data_atualizacao: function() {
                return dateFormat(this.updatedAt, "dd/mm/yyyy h:MM:ss");
            }
        }
    });

    Parte.belongsTo(Processo);
    Movimentacao.belongsTo(Processo);

    // { force: true } para recriar tabelas.
    Processo.sync();
    Parte.sync();
    Movimentacao.sync();

    return {
        Processo: Processo,
        Parte: Parte,
        Movimentacao: Movimentacao,
        QueryInterface: sequelize.getQueryInterface(),
        Connection: function() {
            return getConnection();
        }
    };

    sequelize.close();
}