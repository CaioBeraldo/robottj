'use strict';

var migration = require('migration');

module.exports = migration(a,
    function() {
        Qi.addColumn('processo', 'situacao', { type: Sequelize.STRING });
        Qi.addColumn('processo', 'local_fisico', { type: Sequelize.STRING });
        Qi.addColumn('processo', 'outros_assuntos', { type: Sequelize.STRING });
    },
    function() {
        Qi.removeColumn('processo', 'situacao');
        Qi.removeColumn('processo', 'local_fisico');
        Qi.removeColumn('processo', 'outros_assuntos');
    });
};