'use strict';

var Bluebird = require('bluebird');
var Sequelize = require('sequelize');

module.exports = function(Qi, up, down) {
    return {
        up: function() {
            return new Bluebird(function(resolve, reject) {
                // Describe how to achieve the task.
                // Call resolve/reject at some point.
                try {
                    up();
                    resolve();
                } catch (error) {
                    reject();
                }
            });
        },
        down: function() {
            return new Bluebird(function(resolve, reject) {
                // Describe how to revert the task.
                // Call resolve/reject at some point.
                down();
                resolve();
            });
        }
    }
};