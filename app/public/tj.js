'use strict';

var parser = new DOMParser();
var processosConsultados = [];

String.prototype.format = function(format) {
    var args = Array.prototype.slice.call(arguments, 1);
    return format.replace(/{(\d+)}/g, function(match, number) {
        return typeof args[number] != 'undefined' ? args[number] : match;
    });
};

function sleep(seconds) {
    var start = new Date().getTime();
    while ((new Date().getTime() - start) <= (seconds * 1000)) { continue; }
}

function GetProcessos() {
    $('button').prop('disabled', true);

    var consultarProcessos = document.getElementById('processos').children;
    sendRequest(consultarProcessos, 0);
}

function sendRequest(dataArray, index) {
    if (index >= dataArray.length) {
        // remover em release
        // console.log('pesquisa concluida');
        // console.log(processosConsultados);

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open("POST", "/tj", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(processosConsultados));
        return;
    }

    var numeroProcesso = dataArray[index].textContent.trim();

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.addEventListener("readystatechange", function() {
        try {
            var processos = {
                partes: [],
                movimentacoes: []
            };

            if (this.readyState === 4) {
                var doc = parser.parseFromString(this.response, "text/html");

                if (doc.getElementById("spwTabelaMensagem")) {
                    next();
                    return;
                }

                var partes = undefined;
                if (doc.getElementById("tableTodasPartes"))
                    partes = doc.getElementById("tableTodasPartes").children.item(0).children;
                else
                if (doc.getElementById("tablePartesPrincipais"))
                    partes = doc.getElementById("tablePartesPrincipais").children.item(0).children;
                else
                    partes = [];

                var processo = doc.getElementsByClassName("secaoFormBody").item(1).children.item(0).children;
                var movimentacoes = doc.getElementById("tabelaTodasMovimentacoes").children.item(0).children;

                processos.numero = processo.item(0).children.item(1).children.item(0).children.item(0).children.item(0).children.item(0).children.item(0).textContent.trim();

                // campo novo
                processos.situacao = processo.item(0).children.item(1).children.item(0).children.item(0).children.item(0).children.item(0).children.item(1).textContent.trim();

                // pula uma linha em alguns casos
                // verificar se possui "Classe:" antes de pegar valor
                processos.classe = processo.item(1).children.item(1).children.item(0).children.item(0).children.item(0).children.item(0).children.item(0).textContent.trim();

                processos.area = processo.item(2).children.item(1).children.item(0).children.item(0).children.item(0).children.item(0).lastChild.textContent.trim();
                processos.assunto = processo.item(3).children.item(1).children.item(0).textContent.trim();

                // campo novo
                // adicionar local fisico antes da distribuição.
                processos.local_fisico = undefined;
                // campo novo
                // em alguns casos possui "Outros Assuntos:".
                processos.outros_assuntos = undefined;

                processos.data_distribuicao = processo.item(4).children.item(1).children.item(0).textContent.trim();
                processos.desc_distribuicao = processo.item(5).children.item(1).children.item(0).textContent.trim();
                processos.controle = processo.item(6).children.item(1).children.item(0).textContent.trim();
                processos.juiz = processo.item(7).children.item(1).children.item(0).textContent.trim();
                processos.valor_acao = parseFloat(processo.item(8).children.item(1).children.item(0).textContent.replace('R$', '').replace('.', '').replace(',', '.').trim());

                for (var i = 0; i < partes.length; i++) {
                    var parte = {};

                    parte.tipo = partes.item(i).children.item(0).children.item(0).textContent.trim();
                    parte.nome = partes.item(i).children.item(1).firstChild.textContent.trim();
                    parte.advogado = "";

                    var advogado = partes.item(i).children.item(1).children.item(1); //.textContent.trim();
                    if (advogado != null) // não possui advogado
                        parte.advogado = partes.item(i).children.item(1).lastChild.textContent.trim(); // advogado da parte

                    // Verificar
                    // Partes possuem advogados (subitem no HTML).
                    // Alguns não estão sendo salvos.
                    // Criar vinculo pois advogados estão vinculados a Reqte/Reqdo
                    // Ex.: SELECT * FROM processo WHERE numero = '0000002-21.2014.8.26.0279';

                    processos.partes.push(parte);
                }

                for (var i = 0; i < movimentacoes.length; i++) {
                    var movimentacao = {};

                    movimentacao.data = new Date(movimentacoes.item(i).children.item(0).textContent.trim());
                    movimentacao.observacoes = movimentacoes.item(i).children.item(2).children.item(1).textContent.trim();

                    // caso a movimentação possua documento vinculado (a href do domecumento).
                    if (movimentacoes.item(i).children.item(2).children.item(0).tagName == 'A') {
                        movimentacao.url = movimentacoes.item(i).children.item(2).children.item(0).attributes.href.value;
                        movimentacao.descricao = movimentacoes.item(i).children.item(2).children.item(0).textContent.trim();
                    } else {
                        movimentacao.url = "";
                        movimentacao.descricao = movimentacoes.item(i).children.item(2).firstChild.textContent.trim();
                    }

                    // Verificar como salvar os documentos das movimentações.

                    processos.movimentacoes.push(movimentacao);
                }

                // console.log('salvando processo ' + processos.processo.numero)
                processosConsultados.push(processos);

                next();
            }
        } catch (except) {
            next();
        }

        function next() {
            var width = ((100 * (index + 1)) / dataArray.length);
            $('#progressBar').attr('aria-valuenow', index + 1);
            $('#progressBar').attr('style', 'width:' + width + '%');
            $('#progressBar').text(width.toFixed(2) + '%');
            sendRequest(dataArray, ++index);
        }
    });

    var cdLocal = numeroProcesso.substring(numeroProcesso.length - 4, numeroProcesso.length);
    var nrDigitoAnoUnificado = numeroProcesso.substring(0, numeroProcesso.length - 4);

    var requestData =
        "https://esaj.tjsp.jus.br/cpopg/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=" + cdLocal + "&" +
        "cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=" + nrDigitoAnoUnificado + "&" +
        "foroNumeroUnificado=" + cdLocal + "&dadosConsulta.valorConsultaNuUnificado=" + numeroProcesso + "&" +
        "dadosConsulta.valorConsulta=&uuidCaptcha=sajcaptcha_867858a2c9cd4e97b5082149998a8663";
    xhr.open("GET", requestData, true);
    xhr.send();
}