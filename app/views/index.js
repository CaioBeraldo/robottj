const views = './app/views';
const fs = require('fs');

module.exports = function(app) {
    fs.readdir(views, (err, files) => {
        files.forEach(file => {
            if (file.endsWith('.pug')) {
                var routeName = file.replace('.pug', '');
                app.get('/' + routeName, function(req, res) {
                    res.render(routeName);
                });
                console.log('route created at ' + routeName);
            }
        });
    });
};