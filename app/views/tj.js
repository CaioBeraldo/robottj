const util = require('util');
const dateFormat = require('dateformat');
var fs = require('fs');
var xlsx = require('node-xlsx');

module.exports = function(app, db) {

    app.post('/carregarProcessos', function(req, res) {
        var isEmpty = true;
        for (var i in req.files) {
            if (req.files.hasOwnProperty(i)) {
                isEmpty = false;
                break;
            }
        }

        if (isEmpty)
            res.status(400).send('Nenhum arquivo enviado...');

        var file = req.files.consultarProcessos;
        var processos = xlsx.parse(file.data)[0].data;
        var data = [];

        processos.forEach(function(row) {
            if (row[0] != "")
                data.push(row[0]);
        });

        res.render('processos', { processos: data });
    });

    app.post('/tj', function(req, res) {
        var processos = req.body;

        processos.forEach(function(processo) {
            var connection = db.Connection();

            connection.transaction(function(t) {
                    db.Processo
                        .findOne({ where: { numero: processo.numero } })
                        .then(function(row) {
                            if (!row) {
                                db.Processo
                                    .create(processo, { transaction: t })
                                    .then(function(reg) {
                                        salvarDetalhes(reg, processo);
                                    }, { transaction: t });
                                // .catch(function(err) { console.log(err); });
                            } else {
                                db.Processo
                                    .update(processo, { where: { numero: row.get('numero') } })
                                    .then(function() {
                                        salvarDetalhes(row, processo);
                                    }, { transaction: t });
                                // .catch(function(err) { console.log(err); });
                            }
                        });
                })
                .then(function(result) { console.log(result); })
                .catch(function(error) { console.log(error); });
        });

        function salvarDetalhes(row, processo) {
            db.Parte.destroy({ where: { processoId: row.get('id') } });
            processo.partes.forEach(function(parte) {
                parte.processoId = row.get('id');
                db.Parte.create(parte);
            });

            db.Movimentacao.destroy({ where: { processoId: row.get('id') } });
            processo.movimentacoes.forEach(function(movimentacao) {
                movimentacao.processoId = row.get('id');
                db.Movimentacao.create(movimentacao);
            });
        }

    });

    app.get('/consultar', function(req, res) {
        var offset = req.query.pagina;
        var limit = 20;

        if (offset)
            offset = parseInt(offset);
        else
            offset = 0

        if (offset < 0) offset = 0;

        db.Processo
            .findAndCountAll({ limit: limit, offset: offset * limit })
            .then(function(data) {
                res.render('consultar', { processos: data, offset: offset, limit: limit });
            });
    });

    app.get('/visualizar', function(req, res) {
        var processoId = req.query.id;

        db.Processo
            .findOne({ where: { id: processoId } })
            .then(function(data) {
                var processo = data;

                db.Parte
                    .findAll({ where: { processoId: processo.get('id') } })
                    .then(function(data) {
                        var partes = data;

                        db.Movimentacao
                            .findAll({ where: { processoId: processoId } })
                            .then(function(data) {
                                var movimentacoes = data;
                                res.render('visualizar', { processo: processo, partes: partes, movimentacoes: movimentacoes });
                            });
                    });
            });
    });

}