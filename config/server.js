// importação de módulos
// var contentType = require('content-type');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var getRawBody = require('raw-body');
var expressValidator = require('express-validator');
const fileUpload = require('express-fileupload');

// configurações do servidor
app.set('view engine', 'pug'); // trabalhando com pug para renderização de páginas e templates
app.set('views', './app/views');

// insclusão de middlewares
app.use(express.static('./app/public'));
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '1gb' }));
app.use(expressValidator());
app.use(fileUpload());
app.use(function(req, res, next) {
    // habilitando CORS.
    req.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", "*");
    // res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
});

// app.all('/', function (req, res, next) {
//   console.log('Accessing the common section ...');
//   next(); // pass control to the next handler
// });

module.exports = function() {
    return app;
}