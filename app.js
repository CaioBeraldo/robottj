var app = require('./config/server')();
var routes = require('./app/views')(app);
var db = require("./custom_modules/db")();
// var migration = require('./config/migration');
var tj = require('./app/views/tj')(app, db);
var Umzug = require('umzug');
var umzug = new Umzug({});

// console.log(db.Database);
// migration.up(db.QueryInterface);

var port = process.env.PORT || 3000;

app.get('/', function(req, res) {
    res.redirect('/home');
});

app.listen(port, function() {
    console.log('Server started...')
});

umzug.execute({
    migrations: ['201704101153'],
    method: 'up'
}).then(function(migrations) {
    // "migrations" will be an Array of all executed/reverted migrations.
});